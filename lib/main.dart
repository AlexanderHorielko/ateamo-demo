import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';

import 'package:ateamo_mobile/src/bloc/simple_bloc_observer.dart';
import 'package:ateamo_mobile/src/app.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  WidgetsFlutterBinding.ensureInitialized();

  runApp(App());
}
