import 'package:ateamo_mobile/src/models/event_reminder/match_card_model.dart';

class ScheduleBlocModel {
  String error;
  Map<DateTime, List<MatchCardModel>> dailyMatches;

  ScheduleBlocModel({
    this.dailyMatches,
    this.error,
  });
}
