import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_bloc.dart';
import 'package:ateamo_mobile/src/helpers/helper_functions.dart';
import 'package:ateamo_mobile/src/models/event/competition.dart';
import 'package:ateamo_mobile/src/models/event/rsvps.dart';
import 'package:ateamo_mobile/src/models/team.dart';
import 'package:ateamo_mobile/src/models/location_info.dart';

class EventItem {
  final int id;
  final String type;
  final String eventClass;
  final Team home;
  final Team visitor;
  final int ins;
  final DateTime date;
  final DateTime time;
  final LocationInfo location;
  RSVPStatus myRsvp;
  final Rsvps rsvps;
  final Competition competition;

  EventItem({
    this.id,
    this.type,
    this.eventClass,
    this.home,
    this.visitor,
    this.ins,
    this.date,
    this.time,
    this.location,
    this.myRsvp,
    this.rsvps,
    this.competition,
  });



  factory EventItem.fromJson(Map<String, dynamic> data) {
    return EventItem(
      id: data['id'],
      type: data['type'],
      eventClass: data['class'],
      home: Team.fromJson(data['home']),
      visitor: Team.fromJson(data['visitor']),
      ins: data['ins'],
      date: HelperFunctions.getDateFromTimestamp(data['date']),
      time: HelperFunctions.getDateFromTimestamp(data['time']),
      location: LocationInfo.fromJson(data['location']),
      myRsvp: EventReminderBloc.convertStringToStatus(data['myRsvp'])?? RSVPStatus.rsvpNa,
      rsvps: data['rsvps'] != null ? Rsvps.fromJson(data['rsvps']): Rsvps(),
      competition: data['competition'] != null ? Competition.fromJson(data['competition']) : Competition(),
    );
  }
}
