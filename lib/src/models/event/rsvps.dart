import 'package:ateamo_mobile/src/models/user/user.dart';

class Rsvps {
  final List<User> inList;
  final List<User> maybeList;
  final List<User> outList;
  final List<User> naList;

  Rsvps({
    this.inList,
    this.maybeList,
    this.outList,
    this.naList,
  });

  factory Rsvps.fromJson(Map<String, dynamic> data) {
    return Rsvps(
      inList: _getUsersList(data['in']),
      maybeList: _getUsersList(data['maybe']),
      outList: _getUsersList(data['out']),
      naList: _getUsersList(data['na']),
    );
  }

  static List<User> _getUsersList(List<dynamic> data){
    return data.map((e) => User.fromJson(e)).toList();
  }
}
