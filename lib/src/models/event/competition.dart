class Competition {
  final int id;
  final String name;
  final String nameShort;
  final String season;
  final int awards;
  final String distance;
  final String calories;
  final String image;

  Competition({
    this.id,
    this.name,
    this.nameShort,
    this.season,
    this.awards,
    this.calories,
    this.distance,
    this.image,
  });

  factory Competition.fromJson(Map<String, dynamic> data) {
    return Competition(
      id: data['id'],
      name: data['name'],
      nameShort: data['name_short'],
      season: data['season'],
      awards: data['awards'],
      calories: data['calories'],
      distance: data['distance'],
      image: data['img'],
    );
  }
}
