import 'package:ateamo_mobile/src/models/event_reminder/address_model.dart';

class LocationInfo {
  final double speed;
  final double time;
  final double lat;
  final double long;
  final AddressModel address;
  final String name;
  final String field;
  final String detail;

  LocationInfo({
    this.speed,
    this.time,
    this.lat,
    this.long,
    this.address,
    this.name,
    this.field,
    this.detail,
  });

  factory LocationInfo.fromJson(Map<String, dynamic> data) {
    return LocationInfo(
      address: AddressModel.fromJson(data['address']),
      name: data['name'],
      lat: data['coordinates']['lat'],
      long: data['coordinates']['lon'],
      detail: data['detail'],
      field: data['field'],
    );
  }
}
