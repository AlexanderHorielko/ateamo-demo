import 'package:flutter/material.dart';
import 'package:ateamo_mobile/src/extensions/hex_color.dart';

class Team {
  String name;
  String badge;
  final List<dynamic> recent;
  final List<dynamic> colors;

  Team({
    @required this.badge,
    @required this.name,
    this.recent,
    this.colors,
  });

  factory Team.fromJson(Map<String, dynamic> data) {
    List<dynamic> colors = data['colors']?.map((e) => HexColor(e))?.toList();

    return Team(
      badge: data['img'],
      name: data['name'],
      colors: colors ?? [],
      recent: data['recent'] ?? [],
    );
  }
}
