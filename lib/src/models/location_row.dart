import 'package:ateamo_mobile/src/models/location_info.dart';

class LocationRow {
  final LocationInfo locationInfo;
  final String date;

  LocationRow({ this.locationInfo, this.date });

  static LocationRow fromMap(Map<String, dynamic> data) {
    LocationInfo info = LocationInfo(
      speed: data['speed'],
      time: data['time'],
      lat: data['lat'],
      long: data['long'],
    );

    return LocationRow(
      locationInfo: info,
      date: data['date'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'speed': locationInfo.speed,
      'time': locationInfo.time,
      'lat': locationInfo.lat,
      'long': locationInfo.long,
      'date': date,
    };
  }

  String toString() {
    return '{ speed: ${locationInfo.speed}, time: ${locationInfo.time}, lat: ${locationInfo.lat}, long: ${locationInfo.long}, date: ${date} }';
  }
}
