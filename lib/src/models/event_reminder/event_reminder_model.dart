import 'package:ateamo_mobile/src/models/event_reminder/post_model.dart';
import 'package:ateamo_mobile/src/models/event_reminder/reminder_model.dart';

class EventReminderModel {
  String error;
  List<PostModel> posts;
  List<ReminderModel> reminders;
  int rsvpButtonIndex;

  EventReminderModel({
    this.posts,
    this.reminders,
    this.error,
    this.rsvpButtonIndex,
  });
}
