import 'package:ateamo_mobile/src/helpers/helper_functions.dart';
import 'package:ateamo_mobile/src/models/team.dart';
import 'package:flutter/foundation.dart';

class MatchCardModel {
  final Team homeTeam;
  final Team visitorTeam;
  final DateTime date;
  final DateTime time;
  final String location;

  MatchCardModel({
    @required this.date,
    @required this.homeTeam,
    @required this.visitorTeam,
    @required this.location,
    @required this.time,
  });

  factory MatchCardModel.fromJson(Map<String, dynamic> data) {
    return MatchCardModel(
      date: HelperFunctions.getDateFromTimestamp(
        data['date'],
      ),
      homeTeam: Team(
        badge: data['home']['img'],
        name: data['home']['name'],
      ),
      visitorTeam: Team(
        badge: data['visitor']['img'],
        name: data['visitor']['name'],
      ),
      location: data['location']['name'],
      time: HelperFunctions.getDateFromTimestamp(
        data['time'],
      ),
    );
  }
}
