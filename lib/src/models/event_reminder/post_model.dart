import 'package:ateamo_mobile/src/helpers/helper_functions.dart';
import 'package:ateamo_mobile/src/models/event_reminder/actions_model.dart';
import 'package:ateamo_mobile/src/models/event_reminder/member_model.dart';

class PostModel {
  final String imageUrl;
  final MemberModel member;
  final String caption;
  final DateTime date;
  final ActionsModel actions;

  PostModel({
    this.imageUrl,
    this.member,
    this.caption,
    this.date,
    this.actions,
  });

  factory PostModel.fromJson(Map<String, dynamic> data) {
    return PostModel(
      imageUrl: data['image']['url'],
      date: HelperFunctions.getDateFromTimestamp(data['date']),
      caption: data['caption'],
      actions: ActionsModel.fromJson(data['actions']),
      member: MemberModel.fromJson(data['member']),
    );
  }
}
