import 'package:flutter/material.dart';

class ActionsModel {
  final int likes;
  final List replies;

  ActionsModel({
    @required this.likes,
    @required this.replies,
  });

  factory ActionsModel.fromJson(Map<String, dynamic> data) {
    return ActionsModel(
      likes: data['likes'],
      replies: data['replies'],
    );
  }
}
