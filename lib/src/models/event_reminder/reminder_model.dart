import 'package:ateamo_mobile/src/helpers/helper_functions.dart';
import 'package:ateamo_mobile/src/models/event/event_item.dart';
import 'package:ateamo_mobile/src/models/event_reminder/actions_model.dart';
import 'package:ateamo_mobile/src/models/event_reminder/member_model.dart';
import 'package:flutter/material.dart';

class ReminderModel {
  final String image;
  final EventItem event;
  final MemberModel member;
  final String caption;
  final DateTime date;
  final ActionsModel actions;

  ReminderModel({
    @required this.image,
    @required this.event,
    @required this.member,
    @required this.caption,
    @required this.date,
    @required this.actions,
  });

  factory ReminderModel.fromJson(Map<String, dynamic> data) {
    return ReminderModel(
      image: data['image'],
      event: EventItem.fromJson(data['event']),
      member: MemberModel.fromJson(data['member']),
      caption: data['caption'],
      date: HelperFunctions.getDateFromTimestamp(
        data['date'],
      ),
      actions: ActionsModel.fromJson(data['actions']),
    );
  }
}
