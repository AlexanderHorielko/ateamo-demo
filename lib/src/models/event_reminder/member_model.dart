import 'package:flutter/material.dart';

class MemberModel {
  final int id;
  final String name;
  final String img;

  MemberModel({
    @required this.id,
    @required this.name,
    @required this.img,
  });

  factory MemberModel.fromJson(Map<String, dynamic> data) {
    return MemberModel(
      id: data['id'],
      img: data['img'],
      name: data['username'],
    );
  }
}
