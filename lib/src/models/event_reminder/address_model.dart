import 'package:flutter/material.dart';

class AddressModel {
  final String street;
  final String street2;
  final String city;
  final String state;
  final String postal;

  AddressModel({
    @required this.street,
    @required this.street2,
    @required this.city,
    @required this.state,
    @required this.postal,
  });

  factory AddressModel.fromJson(Map<String, dynamic> data) {
    return AddressModel(
      street: data['street'],
      street2: data['street2'],
      city: data['city'],
      state: data['state'],
      postal: data['postal'],
    );
  }
}
