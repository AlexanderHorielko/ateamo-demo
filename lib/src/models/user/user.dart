class User {
  final int id;
  final String username;
  final String image;

  User({
    this.id,
    this.username,
    this.image,
  });

  factory User.fromJson(Map<String, dynamic> data) {
    return User(
      id: data['id'],
      image: data['img'],
      username: data['username'],
    );
  }
}
