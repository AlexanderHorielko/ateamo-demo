import 'package:ateamo_mobile/src/packages/event_item_repository/lib/event_item_repository.dart';
import 'package:ateamo_mobile/src/packages/event_reminder_repository/lib/event_reminder_repository.dart';
import 'package:ateamo_mobile/src/packages/schedule_repository/lib/schedule_repository.dart';
import 'package:ateamo_mobile/src/screens/event/event_screen_wrapper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:ateamo_mobile/src/screens/initial_screen.dart';

import 'bloc/event_reminder_bloc/event_reminder_bloc.dart';

ScheduleRepository _scheduleRepository = ScheduleRepository();
EventReminderRepository _eventReminderRepository = EventReminderRepository();
EventItemRepository _eventItemRepository = EventItemRepository();

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: null,
      child: AppView(),
    );
  }
}

class AppView extends StatefulWidget {
  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<ScheduleRepository>(
          create: (context) => _scheduleRepository,
        ),
        RepositoryProvider<EventReminderRepository>(
          create: (context) => _eventReminderRepository,
        ),
        RepositoryProvider<EventItemRepository>(
          create: (context) => _eventItemRepository,
        ),
      ],
      child: BlocProvider<EventReminderBloc>(
        create: (BuildContext context) => EventReminderBloc(
          eventReminderRepository: _eventReminderRepository,
        ),
        child: MaterialApp(
          navigatorKey: _navigatorKey,
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            backgroundColor: Color(0xFFF1F1F1),
            primaryColor: Color(0xFF2F7339),
            fontFamily: 'Lato',
            scaffoldBackgroundColor: Color(0xFFE6E6E6),
          ),
          home: InitialScreen(),
          routes: {
            EventScreenWrapper.routeName: (context) => EventScreenWrapper(),
          },
        ),
      ),
    );
  }
}
