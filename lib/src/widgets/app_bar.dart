import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_bloc.dart';
import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyAppbar extends StatefulWidget implements PreferredSizeWidget {
  final Widget title;
  final String label;
  final ScrollController scrollController;

  const MyAppbar({Key key, this.title, this.label, this.scrollController})
      : super(key: key);

  @override
  _MyAppbarState createState() => _MyAppbarState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _MyAppbarState extends State<MyAppbar> {
  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 0,
      color: Theme.of(context).scaffoldBackgroundColor,
      child: Container(
        padding: const EdgeInsets.all(10.0),
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Color.fromRGBO(0, 0, 0, 0.05),
              width: 1.0,
              style: BorderStyle.solid,
            ),
          ),
        ),
        child: AppBar(
          centerTitle: true,
          elevation: 0.0,
          leading: widget.label == null
              ? Image.asset(
                  'assets/images/avataaars16.png',
                )
              : null,
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          title: widget.label == null
              ? IconButton(
                  icon: Image.asset('assets/images/AteamoLogoMark.png'),
                  iconSize: 30,
                  onPressed: () async {
                    await widget.scrollController?.animateTo(
                      0.0,
                      duration: const Duration(seconds: 2),
                      curve: Curves.easeIn,
                    );
                  },
                )
              : Text(
                  widget.label,
                  style: TextStyle(
                    color: Color(0xFF555555),
                  ),
                ),
          brightness: Brightness.light,
          actions: <Widget>[
            IconButton(
              color: Colors.black45,
              icon: const Icon(Icons.miscellaneous_services),
              tooltip: 'Next page',
              onPressed: () {
                // openPage(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
