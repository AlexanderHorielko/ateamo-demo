import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_bloc.dart';
import 'package:ateamo_mobile/src/resources/global_colors.dart';
import 'package:flutter/material.dart';

class RsvpButton extends StatelessWidget {
  final String text;
  final RSVPStatus status;
  final Color color;
  final ButtonPosition position;
  final Function onPressed;

  RsvpButton({
    @required this.status,
    @required this.position,
    @required this.onPressed,
    this.color,
    this.text,
  });

  @override
  Widget build(BuildContext context) {
    final buttonColor = color != null
        ? color
        : status == RSVPStatus.rsvpIn
            ? GlobalColors.green
            : status == RSVPStatus.rsvpMaybe
                ? GlobalColors.grey
                : status == RSVPStatus.rsvpOut
                    ? GlobalColors.black
                    : Colors.black;

    final buttonBorderRadius = position == ButtonPosition.center
        ? BorderRadius.zero
        : position == ButtonPosition.whole
            ? BorderRadius.circular(10)
            : BorderRadius.only(
                topLeft: position == ButtonPosition.left
                    ? Radius.circular(10)
                    : Radius.zero,
                bottomLeft: position == ButtonPosition.left
                    ? Radius.circular(10)
                    : Radius.zero,
                bottomRight: position == ButtonPosition.right
                    ? Radius.circular(10)
                    : Radius.zero,
                topRight: position == ButtonPosition.right
                    ? Radius.circular(10)
                    : Radius.zero,
              );
    return Container(
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          padding: EdgeInsets.symmetric(
            vertical: MediaQuery.of(context).size.width * 0.05,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: buttonBorderRadius,
          ),
          elevation: 0,
          primary: buttonColor,
        ),
        child: Text(
          text != null
              ? text
              : EventReminderBloc.convertStatusToString(status),
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.width * 0.045,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}

enum ButtonPosition {
  left,
  right,
  center,
  whole,
}
