import 'package:ateamo_mobile/src/resources/global_colors.dart';
import 'package:flutter/material.dart';

class ActivityHeader extends StatelessWidget {
  final String title;
  final IconData icon;
  const ActivityHeader({Key key, @required this.title, @required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;
    return Row(
      children: [
        SizedBox(
          width: screenSize.width * 0.01,
        ),
        Icon(
          icon,
          size: screenSize.width * 0.036,
          color: GlobalColors.activityHeaderGrey,
        ),
        SizedBox(
          width: screenSize.width * 0.01,
        ),
        Text(
          title.toUpperCase(),
          style: TextStyle(
            color: GlobalColors.activityHeaderGrey,
            fontSize: 12,
          ),
        ),
      ],
    );
  }
}

