import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_bloc.dart';
import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_state.dart';
import 'package:ateamo_mobile/src/widgets/activity/comment_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ActivityCard extends StatelessWidget {
  final int index;

  ActivityCard(this.index);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;
    return Center(
      child: BlocBuilder<EventReminderBloc, EventReminderState>(
        builder: (context, state) {
          if (state.model.posts.isEmpty) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return Container(
            margin: EdgeInsets.symmetric(vertical: 15),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: Image.network(
                    state.model.posts[index].imageUrl,
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenSize.width * 0.036,
                    vertical: screenSize.height * 0.022,
                  ),
                  child: CommentWidget(
                    caption: state.model.posts[index].caption,
                    name: state.model.posts[index].member.name,
                    imageUrl: state.model.posts[index].member.img,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
