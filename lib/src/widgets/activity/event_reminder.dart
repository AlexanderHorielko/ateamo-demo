import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_bloc.dart';
import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_state.dart';
import 'package:ateamo_mobile/src/models/event_reminder/event_reminder_model.dart';
import 'package:ateamo_mobile/src/screens/event/event_screen_wrapper.dart';
import 'package:ateamo_mobile/src/screens/event/widgets/event_data.dart';
import 'package:ateamo_mobile/src/widgets/activity/comment_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import 'activity_header.dart';

class EventReminderCard extends StatefulWidget {
  final int index;

  EventReminderCard(this.index);

  @override
  _EventReminderCardState createState() => _EventReminderCardState();
}

class _EventReminderCardState extends State<EventReminderCard> {
  EventReminderBloc eventReminderBloc;
  @override
  void initState() {
    super.initState();
    eventReminderBloc = BlocProvider.of<EventReminderBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;
    return BlocBuilder<EventReminderBloc, EventReminderState>(
      builder: (BuildContext context, state) {
        if (state.model.reminders.isEmpty) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        final EventReminderModel model = state.model;
        return GestureDetector(
          onTap: () {
            Navigator.maybeOf(context).pushNamed(
              EventScreenWrapper.routeName,
              arguments: {
                'index': widget.index,
              },
            );
          },
          child: Container(
            margin: EdgeInsets.symmetric(
              vertical: 15,
            ),
            child: Column(
              children: [
                ActivityHeader(
                  title: 'Event reminder',
                  icon: Icons.calendar_today,
                ),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.zero,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                            ),
                            color: Color(0xFFDFDFDF),
                          ),
                          height: 200.0,
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 0.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.all(10),
                                  child: Image.network(
                                    model.reminders[widget.index].event.home
                                        .badge,
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.all(10),
                                  child: Image.network(
                                    model.reminders[widget.index].event.visitor
                                        .badge,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: screenSize.width * 0.04,
                          vertical: screenSize.height * 0.03,
                        ),
                        child: Column(
                          children: [
                            EventData(
                              buttonIndex: widget.index,
                              event: state.model.reminders[widget.index].event,
                              isHomeScreen: true,
                            ),
                            CommentWidget(
                              name: state
                                  .model.reminders[widget.index].member.name,
                              caption:
                                  state.model.reminders[widget.index].caption,
                              imageUrl: state
                                  .model.reminders[widget.index].member.img,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}