import 'package:ateamo_mobile/src/resources/global_colors.dart';
import 'package:flutter/material.dart';

class CommentWidget extends StatefulWidget {
  final String name;
  final String caption;
  final String imageUrl;
  CommentWidget({
    Key key,
    @required this.name,
    @required this.caption,
    @required this.imageUrl,
  }) : super(key: key);

  @override
  _CommentWidgetState createState() => _CommentWidgetState();
}

class _CommentWidgetState extends State<CommentWidget> {
  final int maxCaptionLength = 120;
  bool showAllCaption = false;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;

    String commentCaption = widget.caption;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          child: Image.network(
            widget.imageUrl,
            width: screenSize.width * 0.1,
          ),
        ),
        SizedBox(
          width: screenSize.width * 0.02,
        ),
        Expanded(
          child: Column(
            children: [
              Container(
                width: double.infinity,
                child: Text.rich(
                  TextSpan(
                    children: <InlineSpan>[
                      TextSpan(
                        text: widget.name + ' ',
                        style: TextStyle(
                          color: GlobalColors.darkGrey,
                          fontSize: 14,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      TextSpan(
                          text: commentCaption.length > maxCaptionLength &&
                                  !showAllCaption
                              ? commentCaption.substring(0, maxCaptionLength) +
                                  "..."
                              : commentCaption),
                      commentCaption.length > maxCaptionLength
                          ? WidgetSpan(
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    showAllCaption = !showAllCaption;
                                  });
                                },
                                child: Text(
                                  showAllCaption ? ' less' : ' more',
                                  style: TextStyle(
                                    color: GlobalColors.grey,
                                  ),
                                ),
                              ),
                            )
                          : TextSpan(),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: screenSize.height * 0.011,
              ),
              Container(
                width: double.infinity,
                child: Row(
                  children: [
                    Text(
                      '12m',
                      style: TextStyle(
                        color: GlobalColors.darkGrey,
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      width: screenSize.width * 0.02,
                    ),
                    InkWell(
                      onTap: () {},
                      borderRadius: BorderRadius.circular(10),
                      splashColor: Colors.blue.withOpacity(0.2),
                      focusColor: Colors.blue.withOpacity(0.2),
                      hoverColor: Colors.blue.withOpacity(0.2),
                      highlightColor: Colors.blue.withOpacity(0.2),
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: screenSize.width * 0.02,
                          vertical: screenSize.height * 0.007,
                        ),
                        child: Text(
                          'Like',
                          style: TextStyle(
                            color: GlobalColors.darkGrey,
                            fontSize: 12,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {},
                      borderRadius: BorderRadius.circular(10),
                      splashColor: Colors.blue.withOpacity(0.2),
                      focusColor: Colors.blue.withOpacity(0.2),
                      hoverColor: Colors.blue.withOpacity(0.2),
                      highlightColor: Colors.blue.withOpacity(0.2),
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: screenSize.width * 0.02,
                          vertical: screenSize.height * 0.007,
                        ),
                        child: Text(
                          'Reply',
                          style: TextStyle(
                            color: GlobalColors.darkGrey,
                            fontSize: 12,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
