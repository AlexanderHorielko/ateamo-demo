import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_bloc.dart';
import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_event.dart';
import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'rsvp_button.dart';

class RsvpButtonStatus extends StatelessWidget {
  final int index;
  final RSVPStatus status;
  const RsvpButtonStatus({Key key, this.index, this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RsvpButton(
      status: status,
      position: ButtonPosition.whole,
      onPressed: () {
        BlocProvider.of<EventReminderBloc>(context).add(
          EventReminderEventChooseRSVPStatus(
            status: RSVPStatus.rsvpNa,
            index: index,
          ),
        );
      },
    );
  }
}
