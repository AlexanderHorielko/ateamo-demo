import 'package:flutter/material.dart';

class PhotoCard extends StatelessWidget {
  final String count;

  PhotoCard(this.count);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        margin: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(0),
              child: Image.asset('assets/images/SampleTeamPhoto$count.jpg',
                  fit: BoxFit.cover),
            ),
            const ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.red,
                child: Text('AH'),
              ),
              // title: Text('Robert Jackson'),
              subtitle: Text(
                  'Robert Jackson sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore tempor incididunt ut labore… more'),
            ),
          ],
        ),
      ),
    );
  }
}
