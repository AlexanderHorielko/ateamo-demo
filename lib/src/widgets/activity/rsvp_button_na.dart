import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_bloc.dart';
import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_event.dart';
import 'package:ateamo_mobile/src/widgets/activity/rsvp_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RsvpButtonNa extends StatelessWidget {
  final int index;
  const RsvpButtonNa({Key key, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: RsvpButton(
            status: RSVPStatus.rsvpIn,
            position: ButtonPosition.left,
            onPressed: () {
              BlocProvider.of<EventReminderBloc>(context).add(
                EventReminderEventChooseRSVPStatus(
                  status: RSVPStatus.rsvpIn,
                  index: index,
                ),
              );
            },
          ),
        ),
        Expanded(
          child: RsvpButton(
            status: RSVPStatus.rsvpMaybe,
            position: ButtonPosition.center,
            onPressed: () {
              BlocProvider.of<EventReminderBloc>(context).add(
                EventReminderEventChooseRSVPStatus(
                  status: RSVPStatus.rsvpMaybe,
                  index: index,
                ),
              );
            },
          ),
        ),
        Expanded(
          child: RsvpButton(
            status: RSVPStatus.rsvpOut,
            position: ButtonPosition.right,
            onPressed: () {
              BlocProvider.of<EventReminderBloc>(context).add(
                EventReminderEventChooseRSVPStatus(
                  status: RSVPStatus.rsvpOut,
                  index: index,
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
