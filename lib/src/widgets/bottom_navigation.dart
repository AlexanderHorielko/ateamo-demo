import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:ateamo_mobile/src/bloc/navigation_bloc/bloc.dart';

class BottomNavigation extends StatefulWidget {
  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  List<NavigationItem> _items = [];
  int _selectedIndex = 0;

  @override
  void initState() {
    _items = [
      NavigationItem(icon: Icon(Icons.home), text: 'Home'),
      NavigationItem(icon: Icon(Icons.event), text: 'Schedule'),
      NavigationItem(icon: Icon(Icons.supervised_user_circle), text: 'Groups'),
      NavigationItem(icon: Icon(Icons.radio_button_checked), text: 'Tracking'),
      // NavigationItem(icon: Icon(Icons.map), text: 'Map'),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double bottomPadding = MediaQuery.of(context).padding.bottom;

    return Container(
      decoration: BoxDecoration(
          color: Color(0xFFE6E6E6),
          border: Border(
              top: BorderSide(
                  color: Color.fromRGBO(0, 0, 0, 0.05), width: 1.0))),
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(
          left: 20,
          right: 20,
          bottom: bottomPadding == 0.0 ? 5 : bottomPadding,
          top: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: _items.map((item) {
          int itemIndex = _items.indexOf(item);
          return ClipOval(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  _onTap(item.text.toLowerCase());
                  setState(() {
                    _selectedIndex = itemIndex;
                  });
                },
                child: _buildItem(item, itemIndex == _selectedIndex),
              ),
            ),
          );
        }).toList(),
      ),
    );
  }

  Widget _buildItem(NavigationItem item, bool isSelected) {
    return Container(
      padding: EdgeInsets.all(10),
      child: IconTheme(
        data: IconThemeData(
          size: 32,
          color:
              isSelected ? Theme.of(context).primaryColor : Color(0xFF999999),
        ),
        child: item.icon,
      ),
    );
  }

  void _onTap(String screen) {
    BlocProvider.of<NavigationBloc>(context).add(ChangeScreen(screen: screen));
  }
}

class NavigationItem {
  final Icon icon;
  final String text;

  NavigationItem({this.icon, this.text});
}
