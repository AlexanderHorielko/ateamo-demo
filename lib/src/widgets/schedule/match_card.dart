import 'package:ateamo_mobile/src/models/event_reminder/match_card_model.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MatchCard extends StatelessWidget {
  final MatchCardModel card;

  MatchCard({
    @required this.card,
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        IntrinsicHeight(
          child: Container(
            padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.height * 0.01,
              horizontal: MediaQuery.of(context).size.width * 0.03,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(7),
            ),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Image.network(
                            card.homeTeam.badge,
                            width: MediaQuery.of(context).size.width * 0.07,
                          ),
                          Text(
                            card.homeTeam.name,
                            style: TextStyle(
                              fontSize:
                                  MediaQuery.of(context).size.width * 0.033,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          Image.network(
                            card.visitorTeam.badge,
                            width: MediaQuery.of(context).size.width * 0.07,
                          ),
                          Text(
                            card.visitorTeam.name,
                            style: TextStyle(
                              fontSize:
                                  MediaQuery.of(context).size.width * 0.033,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      DateFormat('E, M/d').format(card.date),
                      style: TextStyle(
                        fontSize: MediaQuery.of(context).size.width * 0.033,
                      ),
                    ),
                    Text(
                      DateFormat('jm').format(card.time).toLowerCase(),
                      style: TextStyle(
                        fontSize: MediaQuery.of(context).size.width * 0.038,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      card.location,
                      style: TextStyle(
                        fontSize: MediaQuery.of(context).size.width * 0.033,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
