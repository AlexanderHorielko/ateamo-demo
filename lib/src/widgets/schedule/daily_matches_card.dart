import 'package:ateamo_mobile/src/models/event_reminder/match_card_model.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'match_card.dart';

class DailyMatchesCard extends StatelessWidget {
  final List<MatchCardModel> matches;
  final DateTime date;

  DailyMatchesCard({
    @required this.matches,
    @required this.date,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.01,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(
              bottom: MediaQuery.of(context).size.width * 0.02,
            ),
            child: Text(
              DateFormat('EEEE, MMM d').format(date),
              textAlign: TextAlign.left,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: MediaQuery.of(context).size.width * 0.035,
              ),
            ),
          ),
          for (var i = 0; i < matches.length; i++)
            MatchCard(
              card: matches[i],
            ),
        ],
      ),
    );
  }
}
