import 'package:ateamo_mobile/src/services/location_tracking.dart';
import 'package:flutter/material.dart';

class LocationTrackingWidget extends StatefulWidget {
  @override
  _LocationTrackingWidgetState createState() => _LocationTrackingWidgetState();
}

class _LocationTrackingWidgetState extends State<LocationTrackingWidget> {
  LocationTracking _locationTracking = new LocationTracking();
  bool _isActive = false;

  @override
  void initState() {
    _locationTracking.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          splashColor: Theme.of(context).primaryColor.withOpacity(0.5),
          onTap: () {
            if (_isActive) {
              _locationTracking.stop();
            } else {
              _locationTracking.start();
            }

            setState(() {
              _isActive = !_isActive;
            });
          },
          child: Container(
            color: Theme.of(context).primaryColor,
            width: 120,
            height: 120,
            child: Center(
              child: Text(
                _isActive ? 'Stop' : 'Start',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _locationTracking.stop();
    print('HERE STOPPED');
    super.dispose();
  }
}
