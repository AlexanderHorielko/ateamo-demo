import 'dart:async';

import 'package:flutter/material.dart';
import 'package:ateamo_mobile/src/models/location_row.dart';
import 'package:ateamo_mobile/src/packages/location_repository/lib/location_repository.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class TrackingMap extends StatefulWidget {
  @override
  _TrackingMapState createState() => _TrackingMapState();
}

class _TrackingMapState extends State<TrackingMap> {
  final String accessToken = 'pk.eyJ1IjoiZHNoa2FsIiwiYSI6ImNraWRmZXZwYTB3MWUycmswbnhpczY1Z2IifQ.9q9RII9cz6kZXIP3oH7Hfw';
  List<LocationRow> items = [];
  Completer<GoogleMapController> _controller = Completer();
  Map<PolylineId, Polyline> _mapPolylines = {};

  CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );


  @override
  void initState() {
    fetchData()
     .then((_) {
       _createPoints(items);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        myLocationButtonEnabled: false,
        polylines: Set<Polyline>.of(_mapPolylines.values),
      ),
    );
  }

  Future<void> fetchData() async {
    LocationRepository repository = LocationRepository();
    await repository.init();
    try {
      dynamic records = await repository.retrieveData();
      if (records.length > 0) {
        dynamic tempPoint = records.first;
        GoogleMapController controller = await _controller.future;
        controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(tempPoint.locationInfo.lat, tempPoint.locationInfo.long), zoom: 24)));
      }
      setState(() {
        items = records;
      });
    } catch (error) {
      print(error);
    }
  }

  void _createPoints(List<LocationRow> rows) {
    final String polylineIdVal = 'polyline_id_1';
    final PolylineId polylineId = PolylineId(polylineIdVal);

    List<LatLng> points = [];
    rows.forEach((row) {
      points.add(LatLng(row.locationInfo.lat, row.locationInfo.long));
    });

    final Polyline polyline = Polyline(
      polylineId: polylineId,
      consumeTapEvents: true,
      color: Colors.red,
      width: 5,
      points: points,
    );

    setState(() {
      _mapPolylines[polylineId] = polyline;
    });
  }
}

