import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class EventReminderEvent extends Equatable {
  const EventReminderEvent();

  @override
  List<Object> get props => [RSVPStatus.rsvpIn];
}

class EventReminderEventChooseRSVPStatus extends EventReminderEvent {
  final RSVPStatus status;
  final int index;

  EventReminderEventChooseRSVPStatus({
    @required this.status,
    @required this.index,
  });
}


class EventReminderEventGetData extends EventReminderEvent {}
