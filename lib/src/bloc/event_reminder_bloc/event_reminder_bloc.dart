import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_event.dart';
import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_state.dart';
import 'package:ateamo_mobile/src/models/event_reminder/event_reminder_model.dart';
import 'package:ateamo_mobile/src/models/event_reminder/post_model.dart';
import 'package:ateamo_mobile/src/models/event_reminder/reminder_model.dart';
import 'package:ateamo_mobile/src/packages/event_reminder_repository/lib/event_reminder_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EventReminderBloc extends Bloc<EventReminderEvent, EventReminderState> {
  EventReminderModel model = EventReminderModel(
    posts: <PostModel>[],
    reminders: <ReminderModel>[],
    rsvpButtonIndex: 0,
  );
  bool refresh = true;
  final EventReminderRepository eventReminderRepository;
  EventReminderBloc({@required this.eventReminderRepository})
      : assert(eventReminderRepository != null),
        super(EventReminderState.initial());

  @override
  Stream<EventReminderState> mapEventToState(EventReminderEvent event) async* {
    try {
      if (event is EventReminderEventChooseRSVPStatus) {
        model.rsvpButtonIndex = event.index;
        model.reminders[event.index].event.myRsvp = event.status;
        refresh = !refresh;
        switch (event.status) {
          case RSVPStatus.rsvpNa:
            yield EventReminderState.statusNa(model, refresh);
            break;
          case RSVPStatus.rsvpIn:
            yield EventReminderState.statusIn(model, refresh);
            break;
          case RSVPStatus.rsvpMaybe:
            yield EventReminderState.statusMaybe(model, refresh);
            break;
          case RSVPStatus.rsvpOut:
            yield EventReminderState.statusOut(model, refresh);
            break;
          default:
            yield EventReminderState.statusNa(model, refresh);
        }
      } else if (event is EventReminderEventGetData) {
        yield EventReminderState.loading(model);
        final activitiesData = await eventReminderRepository.getActivities();
        model.posts = activitiesData['posts'];
        model.reminders = activitiesData['reminders'];
        yield EventReminderState.getData(model, refresh);
      }
    } catch (e) {
      refresh = !refresh;
      model.error = e.toString();
      yield EventReminderState.error(model, refresh);
    }
  }

  static String convertStatusToString(RSVPStatus status) {
    switch (status) {
      case RSVPStatus.rsvpNa:
        return 'NOT AVAILABLE';
      case RSVPStatus.rsvpIn:
        return 'I`M IN';
      case RSVPStatus.rsvpMaybe:
        return 'MAYBE';
      case RSVPStatus.rsvpOut:
        return 'I`M OUT';
    }
    return '';
  }

  static RSVPStatus convertStringToStatus(String status) {
    if (status == 'NOT AVAILABLE' || status == 'na') {
      return RSVPStatus.rsvpNa;
    } else if (status == 'I`M IN' || status == 'in') {
      return RSVPStatus.rsvpIn;
    } else if (status == 'MAYBE' || status == 'maybe') {
      return RSVPStatus.rsvpMaybe;
    } else if (status == 'I`M OUT' || status == 'out') {
      return RSVPStatus.rsvpOut;
    }
    return RSVPStatus.rsvpNa;
  }

}

enum RSVPStatus {
  rsvpNa,
  rsvpIn,
  rsvpMaybe,
  rsvpOut,
}
