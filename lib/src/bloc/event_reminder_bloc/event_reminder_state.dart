import 'package:ateamo_mobile/src/models/event_reminder/event_reminder_model.dart';
import 'package:ateamo_mobile/src/models/event_reminder/post_model.dart';
import 'package:ateamo_mobile/src/models/event_reminder/reminder_model.dart';
import 'package:equatable/equatable.dart';

class EventReminderState extends Equatable {
  final EventReminderModel model;
  final bool refresh;
  final bool loading;
  EventReminderState({
    this.model,
    this.refresh,
    this.loading = false,
  });
  @override
  List<Object> get props => [model, refresh, loading];

  factory EventReminderState.initial() {
    final EventReminderModel model = EventReminderModel(
      posts: <PostModel>[],
      reminders: <ReminderModel>[],
      rsvpButtonIndex: 0,
    );
    return EventReminderState(
      model: model,
    );
  }
  factory EventReminderState.updateRSVPStatus(
      EventReminderModel model, bool refresh) {
    return EventReminderState(
      model: model,
      refresh: refresh,
    );
  }
  factory EventReminderState.updatedRSVPStatus(
      EventReminderModel model, bool refresh) {
    return EventReminderState(
      model: model,
      refresh: refresh,
    );
  }
  factory EventReminderState.statusIn(EventReminderModel model, bool refresh) {
    return EventReminderState(
      model: model,
      refresh: refresh,
    );
  }
  factory EventReminderState.statusNa(EventReminderModel model, bool refresh) {
    return EventReminderState(
      model: model,
      refresh: refresh,
    );
  }
  factory EventReminderState.statusMaybe(
      EventReminderModel model, bool refresh) {
    return EventReminderState(
      model: model,
      refresh: refresh,
    );
  }
  factory EventReminderState.statusOut(EventReminderModel model, bool refresh) {
    return EventReminderState(
      model: model,
      refresh: refresh,
    );
  }
  factory EventReminderState.getData(EventReminderModel model, bool refresh) {
    return EventReminderState(
      model: model,
      refresh: refresh,
    );
  }
  factory EventReminderState.error(EventReminderModel model, bool refresh) {
    return EventReminderState(
      model: model,
      refresh: refresh,
    );
  }
  factory EventReminderState.loading(EventReminderModel model) {
    return EventReminderState(
      model: model,
      loading: true,
    );
  }
}
