import 'package:equatable/equatable.dart';

abstract class NavigationState extends Equatable {
  const NavigationState();

  @override
  List<Object> get props => [];
}

class NavigationStateInitial extends NavigationState {}

class HomePageLoadedState extends NavigationState {}

class TrackingPageLoadedState extends NavigationState {}

class SchedulePageLoadedState extends NavigationState {}

class GroupPageLoadedState extends NavigationState {}

class MapPageLoadedState extends NavigationState {}
