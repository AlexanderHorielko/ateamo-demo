import 'package:bloc/bloc.dart';

import 'package:ateamo_mobile/src/bloc/navigation_bloc/navigation_event.dart';
import 'package:ateamo_mobile/src/bloc/navigation_bloc/navigation_state.dart';

class NavigationBloc extends Bloc<NavigationEvent, NavigationState> {
  NavigationBloc() : super(HomePageLoadedState());

  @override
  Stream<NavigationState> mapEventToState(NavigationEvent event) async* {
    print('event => ${event}');
    if (event is ChangeScreen) {
      print('event.screen => ${event.screen}');
      switch (event.screen) {
        case 'home':
          yield HomePageLoadedState();
          break;
        case 'tracking':
          yield TrackingPageLoadedState();
          break;
        case 'schedule':
          yield SchedulePageLoadedState();
          break;
        case 'groups':
          yield GroupPageLoadedState();
          break;
        case 'map':
          yield MapPageLoadedState();
          break;
        default:
          yield NavigationStateInitial();
      }
    }
  }
}
