import 'package:equatable/equatable.dart';

abstract class NavigationEvent extends Equatable {
  const NavigationEvent();

  @override
  List<Object> get props => [];
}

class ChangeScreen extends NavigationEvent {
  final String screen;

  ChangeScreen({ this.screen });

  @override
  List<Object> get props => [screen];
}
