import 'package:ateamo_mobile/src/bloc/event_item_bloc/event_item_event.dart';
import 'package:ateamo_mobile/src/models/event/event_bloc_model.dart';
import 'package:ateamo_mobile/src/packages/event_item_repository/lib/event_item_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

import 'event_item_state.dart';

class EventItemBloc extends Bloc<EventItemEvent, EventItemState> {
  final EventItemRepository eventItemRepository;
  EventItemBloc({@required this.eventItemRepository})
      : assert(eventItemRepository != null),
        super(EventItemState.initalState());

  EventBlocModel model = EventBlocModel();
  bool refresh = false;

  @override
  Stream<EventItemState> mapEventToState(EventItemEvent event) async* {
    try {
      if (event is EventItemGetEventData) {
        yield EventItemState.loading(model);
        model.event =
            await eventItemRepository.getEventData(eventId: event.index);
        yield EventItemState.fetchedData(model, refresh);
      }
    } catch (e) {
      yield EventItemState.error(
        model,
        e.message ?? e.toString(),
      );
    }
  }
}
