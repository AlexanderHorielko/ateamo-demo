import 'package:equatable/equatable.dart';

abstract class EventItemEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class EventItemGetEventData extends EventItemEvent {
  final int index;

  EventItemGetEventData(this.index);
}
