import 'package:ateamo_mobile/src/models/event/event_bloc_model.dart';
import 'package:equatable/equatable.dart';

class EventItemState extends Equatable {
  final bool refresh;
  final bool loading;
  final String error;
  final EventBlocModel model;

  EventItemState({
    this.refresh,
    this.error,
    this.model,
    this.loading = false,
  });

  @override
  List<Object> get props => [model, error, refresh, loading];

  factory EventItemState.initalState() {
    return EventItemState(
      model: EventBlocModel(),
      refresh: false,
    );
  }

  factory EventItemState.error(EventBlocModel model, String error) {
    return EventItemState(
      model: model,
      error: error,
    );
  }

  factory EventItemState.loading(EventBlocModel model) {
    return EventItemState(
      model: model,
      loading: true,
    );
  }

  factory EventItemState.fetchedData(EventBlocModel model, bool refresh) {
    return EventItemState(
      model: model,
      refresh: refresh,
    );
  }
}
