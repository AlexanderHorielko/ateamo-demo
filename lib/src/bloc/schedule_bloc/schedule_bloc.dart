import 'package:ateamo_mobile/src/bloc/schedule_bloc/schedule_event.dart';
import 'package:ateamo_mobile/src/bloc/schedule_bloc/schedule_state.dart';
import 'package:ateamo_mobile/src/models/event_reminder/match_card_model.dart';
import 'package:ateamo_mobile/src/models/schedule_bloc_model.dart';
import 'package:ateamo_mobile/src/packages/schedule_repository/lib/schedule_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

class ScheduleBloc extends Bloc<ScheduleEvent, ScheduleState> {
  ScheduleBlocModel model = ScheduleBlocModel();
  bool refresh = true;
  final ScheduleRepository scheduleRepository;

  ScheduleBloc({@required this.scheduleRepository})
      : assert(scheduleRepository != null),
        super(ScheduleState.initial());
  @override
  Stream<ScheduleState> mapEventToState(ScheduleEvent event) async* {
    try {
      if (event is ScheduleEventGetMatches) {
        final matches = await scheduleRepository.getAllMathces();
        model.dailyMatches = {};

        matches.forEach((match) {
          final matchDay = _getMatchDate(match);
          if (!model.dailyMatches.containsKey(matchDay)) {
            model.dailyMatches[matchDay] = [];
          }
        });

        final sortedKeys = model.dailyMatches.keys.toList()..sort();
        final Map<DateTime, List<MatchCardModel>> tempMatchesContainer = {};

        sortedKeys.forEach((element) {
          tempMatchesContainer[element] = [];
        });

        matches.forEach((match) {
          final matchDay = _getMatchDate(match);
          tempMatchesContainer[matchDay].add(match);
        });
        model.dailyMatches = tempMatchesContainer;

        refresh = !refresh;
        yield ScheduleState.showMatches(model, refresh);
      }
    } catch (e) {
      model.error = e.toString();
      refresh = !refresh;
      yield ScheduleState.error(model, refresh);
    }
  }

  DateTime _getMatchDate(MatchCardModel match) {
    return DateTime(
      match.date.year,
      match.date.month,
      match.date.day,
    );
  }
}
