import 'package:ateamo_mobile/src/models/schedule_bloc_model.dart';
import 'package:equatable/equatable.dart';

class ScheduleState extends Equatable {
  final ScheduleBlocModel model;
  final bool refresh;
  ScheduleState({
    this.model,
    this.refresh,
  });
  @override
  List<Object> get props => [model, refresh];

  factory ScheduleState.initial() {
    final ScheduleBlocModel model = ScheduleBlocModel();
    return ScheduleState(model: model);
  }
  factory ScheduleState.error(ScheduleBlocModel model, bool refresh) {
    return ScheduleState(model: model, refresh: refresh);
  }
  factory ScheduleState.showMatches(ScheduleBlocModel model, bool refresh) {
    return ScheduleState(model: model, refresh: refresh);
  }
}
