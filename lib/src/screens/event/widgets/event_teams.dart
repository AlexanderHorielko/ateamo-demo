import 'package:ateamo_mobile/src/bloc/event_item_bloc/event_item_bloc.dart';
import 'package:ateamo_mobile/src/bloc/event_item_bloc/event_item_state.dart';
import 'package:ateamo_mobile/src/screens/event/widgets/event_lower_headers.dart';
import 'package:ateamo_mobile/src/screens/event/widgets/event_teams/event_team.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EventTeams extends StatelessWidget {
  const EventTeams({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;

    return Container(
      width: double.infinity,
      height: screenSize.height * 0.22,
      padding: EdgeInsets.only(
        left: screenSize.width * 0.053,
      ),
      child: BlocBuilder<EventItemBloc, EventItemState>(
        builder: (context, state) {
          return Column(
            children: [
              EventLowerHeaders(
                text: 'Teams',
              ),
              SizedBox(
                height: screenSize.height * 0.011,
              ),
              EventTeam(
                team: state.model.event.home,
                label: 'Home team',
              ),
              SizedBox(
                height: screenSize.height * 0.032,
              ),
              EventTeam(
                team: state.model.event.visitor,
                label: 'Visitor team',
              ),
            ],
          );
        },
      ),
    );
  }
}
