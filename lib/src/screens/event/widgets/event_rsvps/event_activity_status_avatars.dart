import 'package:ateamo_mobile/src/models/user/user.dart';
import 'package:flutter/material.dart';


class EventActivityStatusAvatars extends StatelessWidget {
  final List<User> userList;
  const EventActivityStatusAvatars({
    Key key,
    @required this.userList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;
    return Container(
      padding: EdgeInsets.only(
        top: screenSize.height * 0.011,
        right: screenSize.width * 0.068,
      ),
      child: Wrap(
        runSpacing: screenSize.width * 0.016,
        spacing: screenSize.width * 0.016,
        alignment: WrapAlignment.start,
        children: [
          for (var i = 0; i < userList.length; i++)
            Container(
              child: Image.network(
                userList[i].image,
                height: screenSize.height * 0.055,
              ),
            ),
        ],
      ),
    );
  }
}
