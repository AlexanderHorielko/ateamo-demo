import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_bloc.dart';
import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_state.dart';
import 'package:ateamo_mobile/src/models/event/event_item.dart';
import 'package:ateamo_mobile/src/widgets/activity/rsvp_button_na.dart';
import 'package:ateamo_mobile/src/widgets/activity/rsvp_button_status.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class EventData extends StatelessWidget {
  final EventItem event;
  final int buttonIndex;
  final bool isHomeScreen;
  const EventData(
      {Key key, this.event, this.buttonIndex, this.isHomeScreen = false})
      : super(key: key);
  List<Widget> getChildren(Size screenSize) {
    return [
      Text(
        'Date',
        style: TextStyle(
          color: Color(0xFFBCBCBC),
          fontSize: 16,
        ),
      ),
      Text(
        DateFormat('E, MMM d').format(
          event.date,
        ),
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.bold,
          color: Color(0xFF555555),
        ),
      ),
      Text(
        'Time',
        style: TextStyle(
          color: Color(0xFFCCCCCC),
          fontSize: 16,
        ),
      ),
      Text(
        DateFormat('jm').format(event.time).toLowerCase(),
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.bold,
          color: Color(0xFF555555),
        ),
      ),
      Text(
        'Location',
        style: TextStyle(
          color: Color(0xFFCCCCCC),
          fontSize: 16,
        ),
      ),
      Text(
        event.location.address.street,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Color(0xFF555555),
          fontSize: 16,
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;
    return BlocBuilder<EventReminderBloc, EventReminderState>(
      builder: (context, state) {
        final status = state.model.reminders[buttonIndex].event.myRsvp;
        final children = getChildren(screenSize);
        return Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  event.home.name,
                  style: TextStyle(
                    fontSize: 21,
                    height: 1,
                    color: Color(0xFF666666),
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Container(
                child: Text(
                  'vs ' + event.visitor.name,
                  style: TextStyle(
                    fontSize: 21,
                    height: 1,
                    color: Color(0xFF666666),
                    fontWeight: FontWeight.w600,
                  ),
                ),
                alignment: Alignment.topLeft,
              ),
              if (status == RSVPStatus.rsvpNa)
                Padding(
                  padding: EdgeInsets.only(
                    top: screenSize.height * 0.02,
                  ),
                  child: Column(
                    children: [
                      RsvpButtonNa(
                        index: buttonIndex,
                      ),
                    ],
                  ),
                ),
              Container(
                padding: EdgeInsets.only(
                  top: screenSize.height * 0.02,
                ),
                child: status != RSVPStatus.rsvpNa || isHomeScreen
                    ? Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                              bottom: screenSize.height * 0.02
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: children,
                            ),
                          ),
                          status != RSVPStatus.rsvpNa
                              ? Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        width: double.infinity,
                                        child: RsvpButtonStatus(
                                          index: buttonIndex,
                                          status: status,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                          top: screenSize.height * 0.005,
                                        ),
                                        width: screenSize.width * 0.32,
                                        child: Text(
                                          'Need to update your status? Click the button.',
                                          style: TextStyle(
                                            color: Color.fromRGBO(
                                                186, 186, 186, 1),
                                            fontSize: 12,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : Container(),
                        ],
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              children[0],
                              children[1],
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              children[2],
                              children[3],
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              children[4],
                              children[5],
                            ],
                          ),
                        ],
                      ),
              ),
              if (status == RSVPStatus.rsvpNa)
                Container(
                  margin: EdgeInsets.only(
                    top: screenSize.height * 0.01,
                    bottom: screenSize.height * 0.01,
                  ),
                  child: Text(
                    'You have not yet submitted your RSVP.',
                    style: TextStyle(
                      color: Color.fromRGBO(186, 186, 186, 1),
                      fontSize: 12,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
            ],
          ),
        );
      },
    );
  }
}
