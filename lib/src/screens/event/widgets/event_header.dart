import 'package:ateamo_mobile/src/models/event/event_bloc_model.dart';
import 'package:ateamo_mobile/src/resources/global_colors.dart';
import 'package:flutter/material.dart';

class EventHeader extends StatelessWidget {
  final EventBlocModel model;
  final bool isNavbarReduced;
  const EventHeader({Key key, this.model, this.isNavbarReduced})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;

    return Container(
      decoration: BoxDecoration(
        color: Colors.grey[350],
      ),
      padding: EdgeInsets.only(
        top: screenSize.height * 0.04,
        bottom: screenSize.height * 0.005,
      ),
      child: isNavbarReduced
          ? Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _buildBackButton(context),
                Image.network(
                  model.event.home.badge,
                  height: screenSize.height * 0.06,
                ),
                _buildHeaderText(screenSize),
                Image.network(
                  model.event.visitor.badge,
                  height: screenSize.height * 0.06,
                ),
                Opacity(
                  opacity: 0,
                  child: IconButton(
                    icon: Icon(
                      Icons.chevron_left,
                    ),
                    onPressed: () {},
                  ),
                ),
              ],
            )
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: screenSize.height * 0.037),
                Container(
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _buildBackButton(context),
                      Spacer(),
                      _buildHeaderText(screenSize),
                      Spacer(),
                      Opacity(
                        opacity: 0,
                        child: IconButton(
                          icon: Icon(Icons.chevron_left),
                          onPressed: () {},
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenSize.width * 0.1,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.network(
                        model.event.home.badge,
                        width: screenSize.width * 0.15,
                      ),
                      Image.network(
                        model.event.visitor.badge,
                        width: screenSize.width * 0.15,
                      ),
                    ],
                  ),
                )
              ],
            ),
    );
  }

  Widget _buildBackButton(BuildContext context) {
    return Container(
      child: IconButton(
        icon: Icon(
          Icons.chevron_left_outlined,
          color: GlobalColors.darkGrey,
        ),
        onPressed: () {
          Navigator.maybeOf(context).pop();
        },
      ),
    );
  }

  Widget _buildHeaderText(Size screenSize) {
    return Container(
      child: Text(
        'League Match',
        style: TextStyle(
          fontSize: screenSize.width * 0.04,
          color: GlobalColors.darkGrey,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}
