import 'package:flutter/material.dart';

class EventInsNumber extends StatelessWidget {
  final int number;
  final Color color;
  final String label;
  const EventInsNumber({
    Key key,
    @required this.number,
    @required this.color,
    @required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;
    return IntrinsicWidth(
      child: Container(
        height: screenSize.height * 0.026,
        padding: EdgeInsets.symmetric(
          horizontal: screenSize.width * 0.015,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            color: color,
            width: 1,
          ),
        ),
        child: Center(
          child: Text(
            '$number $label',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: color,
              fontSize: 13,
            ),
          ),
        ),
      ),
    );
  }
}
