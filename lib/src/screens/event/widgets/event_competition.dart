import 'package:ateamo_mobile/src/models/event/event_bloc_model.dart';
import 'package:ateamo_mobile/src/resources/global_colors.dart';
import 'package:flutter/material.dart';

import 'event_lower_headers.dart';

class EventCompetition extends StatelessWidget {
  final EventBlocModel model;
  const EventCompetition({Key key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;
    return Container(
      height: screenSize.height * 0.19,
      width: double.infinity,
      padding: EdgeInsets.only(
        left: screenSize.width * 0.053,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          EventLowerHeaders(
            text: 'Competition',
          ),
          SizedBox(
            height: screenSize.height * 0.011,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: screenSize.height * 0.055,
                child: Image.network(model.event.competition.image),
              ),
              SizedBox(
                width: screenSize.width * 0.02,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      model.event.competition.name,
                      style: TextStyle(
                        fontSize: 13,
                        color: GlobalColors.darkGrey,
                      ),
                    ),
                  ),
                  Container(
                    child: Text(
                      model.event.competition.season,
                      style: TextStyle(
                        fontSize: 16,
                        color: GlobalColors.darkGrey,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
