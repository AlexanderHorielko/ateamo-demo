import 'package:ateamo_mobile/src/bloc/event_item_bloc/event_item_bloc.dart';
import 'package:ateamo_mobile/src/bloc/event_item_bloc/event_item_state.dart';
import 'package:ateamo_mobile/src/models/team.dart';
import 'package:ateamo_mobile/src/resources/global_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EventTeam extends StatefulWidget {
  final Team team;
  final String label;

  EventTeam({Key key, this.team, this.label}) : super(key: key);

  @override
  _EventTeamState createState() => _EventTeamState();
}

class _EventTeamState extends State<EventTeam> {
  final GlobalKey _columnKey = GlobalKey();
  Size size;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getSizeAndPosition();
    });
  }

  getSizeAndPosition() {
    RenderBox _cardBox = _columnKey.currentContext.findRenderObject();
    size = _cardBox.size;
    setState(() {});
  }

  String getRecent(List<dynamic> recent) {
    String recentValues = '';
    for (var item in recent) {
      final afterSign = recent.last != item ? '-' : '';
      recentValues += item.toUpperCase() + afterSign;
    }
    return '(' + recentValues + ')';
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;
    return BlocBuilder<EventItemBloc, EventItemState>(
      builder: (context, state) {
        return Container(
          padding: EdgeInsets.only(
            right: screenSize.width * 0.06,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.network(
                widget.team.badge,
                height: size != null ? size.height : 0,
              ),
              SizedBox(
                width: screenSize.width * 0.015,
              ),
              Column(
                key: _columnKey,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.label,
                    style: TextStyle(
                      color: GlobalColors.teamLabelGrey,
                      fontSize: 13,
                    ),
                  ),
                  Text(
                    widget.team.name,
                    style: TextStyle(
                      color: GlobalColors.darkGrey,
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Text(
                    getRecent(widget.team.recent),
                    style: TextStyle(
                      color: GlobalColors.darkGrey,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
              Spacer(),
              SvgPicture.asset(
                'assets/images/tshirt_icon.svg',
                width: screenSize.width * 0.05,
                color: widget.team.colors[0],

              ),
              SizedBox(
                width: screenSize.width * 0.022,
              ),
              SvgPicture.asset(
                'assets/images/tshirt_icon.svg',
                width: screenSize.width * 0.05,
                color: widget.team.colors[1],
              ),
            ],
          ),
        );
      },
    );
  }
}
