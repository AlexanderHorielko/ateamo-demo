import 'package:ateamo_mobile/src/models/event/event_bloc_model.dart';
import 'package:ateamo_mobile/src/resources/global_colors.dart';
import 'package:flutter/material.dart';

import 'event_rsvps/event_activity_status_avatars.dart';
import 'event_ins_number.dart';

class EventRsvps extends StatelessWidget {
  final EventBlocModel model;
  const EventRsvps({Key key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(
        left: screenSize.width * 0.053,
      ),
      decoration: BoxDecoration(
        color: GlobalColors.lightGrey,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Container(
                padding: EdgeInsets.only(
                  top: screenSize.height * 0.018,
                ),
                child: Text(
                  'Activity & RSVPs',
                  style: TextStyle(
                    fontSize: 16,
                    color: GlobalColors.darkGrey,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: screenSize.height * 0.054,
                ),
                child: EventInsNumber(
                  number: model.event.rsvps.inList.length,
                  color: GlobalColors.green,
                  label: 'ins',
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: screenSize.height * 0.01,
                ),
                child: Align(
                  alignment: Alignment.topRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Image.network(
                        model.event.home.badge,
                        height: screenSize.height * 0.055,
                      ),
                      Image.network(
                        model.event.visitor.badge,
                        height: screenSize.height * 0.055,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          EventActivityStatusAvatars(
            userList: model.event.rsvps.inList,
          ),
          SizedBox(
            height: screenSize.height * 0.014,
          ),
          EventInsNumber(
            number: model.event.rsvps.maybeList.length,
            color: GlobalColors.grey,
            label: 'maybe',
          ),
          EventActivityStatusAvatars(
            userList: model.event.rsvps.maybeList,
          ),
          SizedBox(
            height: screenSize.height * 0.014,
          ),
          EventInsNumber(
            number: model.event.rsvps.outList.length,
            color: GlobalColors.black,
            label: 'out',
          ),
          EventActivityStatusAvatars(
            userList: model.event.rsvps.outList,
          ),
          SizedBox(
            height: screenSize.height * 0.027,
          ),
        ],
      ),
    );
  }
}
