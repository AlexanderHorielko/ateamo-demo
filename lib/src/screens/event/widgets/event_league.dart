import 'package:ateamo_mobile/src/bloc/event_item_bloc/event_item_bloc.dart';
import 'package:ateamo_mobile/src/bloc/event_item_bloc/event_item_state.dart';
import 'package:ateamo_mobile/src/resources/global_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

class EventLeague extends StatelessWidget {
  const EventLeague({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;
    return Container(
      height: screenSize.height * 0.26,
      width: double.infinity,
      margin: EdgeInsets.only(
        left: screenSize.width * 0.036,
        right: screenSize.width * 0.036,
      ),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: GlobalColors.darkGrey,
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        width: screenSize.width * 0.73,
        child: BlocBuilder<EventItemBloc, EventItemState>(
          builder: (context, state) {
            final competitionData = state.model.event.competition;
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: screenSize.height * 0.1,
                  child: Image.network(competitionData.image),
                ),
                Container(
                  width: screenSize.width * 0.55,
                  child: Text(
                    competitionData.name,
                    style: TextStyle(
                      color: GlobalColors.darkGrey,
                      fontSize: 22,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: screenSize.height * 0.022,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      children: [
                        Container(
                          child: SvgPicture.asset(
                            'assets/images/shield_star_icon.svg',
                            width: screenSize.width * 0.043,
                          ),
                        ),
                        SizedBox(
                          width: screenSize.width * 0.02,
                        ),
                        Container(
                          child: Text(
                            competitionData.awards.toString(),
                            style: TextStyle(
                              fontSize: 16,
                              color: GlobalColors.darkGrey,
                            ),
                          ),
                        ),
                      ],
                    ),
                    _buildFooterRow(
                      context,
                      icon: Icons.directions_run,
                      data: competitionData.distance,
                    ),
                    _buildFooterRow(
                      context,
                      icon: Icons.favorite,
                      data: competitionData.calories,
                    ),
                  ],
                )
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildFooterRow(BuildContext context, {IconData icon, String data}) {
    final screenSize = MediaQuery.maybeOf(context).size;
    return Row(
      children: [
        Container(
          child: Icon(
            icon,
            size: screenSize.width * 0.043,
          ),
        ),
        SizedBox(
          width: screenSize.width * 0.02,
        ),
        Container(
          child: Text(
            data,
            style: TextStyle(
              fontSize: 16,
              color: GlobalColors.darkGrey,
            ),
          ),
        ),
      ],
    );
  }
}
