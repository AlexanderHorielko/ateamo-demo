import 'package:ateamo_mobile/src/resources/global_colors.dart';
import 'package:flutter/material.dart';

class EventLowerHeaders extends StatelessWidget {
  final String text;
  const EventLowerHeaders({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Text(
        text.toUpperCase(),
        style: TextStyle(
          fontSize: 13,
          color: GlobalColors.eventHeaderGrey,
          fontWeight: FontWeight.w900,
        ),
        textAlign: TextAlign.start,
      ),
    );
  }
}
