import 'package:ateamo_mobile/src/models/event/event_bloc_model.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class EventLocation extends StatelessWidget {
  final EventBlocModel model;

  const EventLocation({Key key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;
    final location = model.event.location;
    return Container(
      height: screenSize.height * 0.29,
      child: Stack(
        children: [
          GoogleMap(
            initialCameraPosition: CameraPosition(
              target: LatLng(
                location.lat,
                location.long + 0.02,
              ),
              zoom: 14,
            ),
            markers: {
              Marker(
                markerId: MarkerId('0'),
                position: LatLng(
                  location.lat,
                  location.long,
                ),
                icon: BitmapDescriptor.defaultMarkerWithHue(
                  BitmapDescriptor.hueGreen,
                ),
              ),
            },
            mapType: MapType.satellite,
            myLocationEnabled: false,
            myLocationButtonEnabled: false,
            padding: EdgeInsets.only(left: screenSize.width),
          ),
          Container(
            padding: EdgeInsets.only(
              left: screenSize.width * 0.04,
              bottom: screenSize.height * 0.035, 
            ),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: IntrinsicHeight(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      location.name,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                    Text(
                      location.address.street + '.',
                      style: TextStyle(color: Colors.white, fontSize: 10),
                    ),
                    Text(
                      '${location.address.city}, ${location.address.state} ${location.address.postal}',
                      style: TextStyle(color: Colors.white, fontSize: 10),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
