import 'package:ateamo_mobile/src/bloc/event_item_bloc/event_item_bloc.dart';
import 'package:ateamo_mobile/src/packages/event_item_repository/lib/event_item_repository.dart';
import 'package:ateamo_mobile/src/screens/event/event_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EventScreenWrapper extends StatefulWidget {
  static const routeName = '/event';
  EventScreenWrapper({Key key}) : super(key: key);

  @override
  _EventScreenWrapperState createState() => _EventScreenWrapperState();
}

class _EventScreenWrapperState extends State<EventScreenWrapper> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: EventItemBloc(
        eventItemRepository:
            RepositoryProvider.of<EventItemRepository>(context),
      ),
      child: EventScreen(
        index: (ModalRoute.of(context).settings.arguments as Map)['index'],
      ),
    );
  }
}
