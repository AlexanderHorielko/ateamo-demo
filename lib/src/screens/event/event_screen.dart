import 'package:ateamo_mobile/src/bloc/event_item_bloc/event_item_bloc.dart';
import 'package:ateamo_mobile/src/bloc/event_item_bloc/event_item_event.dart';
import 'package:ateamo_mobile/src/bloc/event_item_bloc/event_item_state.dart';
import 'package:ateamo_mobile/src/resources/global_colors.dart';
import 'package:ateamo_mobile/src/screens/event/widgets/event_data.dart';
import 'package:ateamo_mobile/src/screens/event/widgets/event_header.dart';
import 'package:ateamo_mobile/src/screens/event/widgets/event_ins_number.dart';
import 'package:ateamo_mobile/src/screens/event/widgets/event_location.dart';
import 'package:ateamo_mobile/src/screens/event/widgets/event_rsvps.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'widgets/event_competition.dart';
import 'widgets/event_league.dart';
import 'widgets/event_teams.dart';

class EventScreen extends StatefulWidget {
  final int index;
  EventScreen({Key key, this.index}) : super(key: key);

  @override
  _EventScreenState createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen>
    with TickerProviderStateMixin {
  EventItemBloc _eventItemBloc;
  ScrollController _scrollController = ScrollController();
  bool isNavbarReduced = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final screenHeight = MediaQuery.maybeOf(context).size.height;
    _eventItemBloc = BlocProvider.of<EventItemBloc>(context);
    _eventItemBloc.add(EventItemGetEventData(widget.index));
    addScrollListener(screenHeight);
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  void addScrollListener(double screenHeight) {
    _scrollController.addListener(() {
      if (_scrollController.offset > screenHeight * 0.05 && !isNavbarReduced) {
        setState(() {
          isNavbarReduced = true;
        });
      } else if (_scrollController.offset < screenHeight * 0.05 &&
          isNavbarReduced) {
        setState(() {
          isNavbarReduced = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.maybeOf(context).size;
    return Scaffold(
      body: BlocBuilder<EventItemBloc, EventItemState>(
        builder: (context, state) {
          if (state.loading || state.model.event == null) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return NestedScrollView(
            controller: _scrollController,
            headerSliverBuilder: (context, innerBoxIsScrolled) {
              return [
                SliverAppBar(
                  expandedHeight: screenSize.height * 0.21,
                  collapsedHeight: screenSize.height * 0.09,
                  floating: true,
                  pinned: true,
                  leading: Container(),
                  backgroundColor: Colors.grey[350],
                  flexibleSpace: FlexibleSpaceBar(
                    titlePadding: EdgeInsets.all(0),
                    centerTitle: true,
                    title: AnimatedSize(
                      duration: const Duration(milliseconds: 150),
                      vsync: this,
                      child: Container(
                        height: screenSize.height * 0.23,
                        child: EventHeader(
                          model: state.model,
                          isNavbarReduced: isNavbarReduced,
                        ),
                      ),
                    ),
                  ),
                ),
              ];
            },
            body: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Stack(
                      children: [
                        EventLocation(model: state.model),
                        Padding(
                          padding: EdgeInsets.only(
                            top: screenSize.height * 0.32,
                            left: screenSize.width * 0.04,
                            right: screenSize.width * 0.04,
                            bottom: screenSize.height * 0.01,
                          ),
                          child: EventData(
                            event: state.model.event,
                            buttonIndex: widget.index,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            top: screenSize.height * 0.275,
                            left: screenSize.width * 0.04,
                          ),
                          child: EventInsNumber(
                            number: state.model.event.ins,
                            color: GlobalColors.green,
                            label: 'ins',
                          ),
                        ),
                      ],
                    ),
                    EventRsvps(model: state.model),
                    EventCompetition(model: state.model),
                    EventTeams(),
                    EventLeague(),
                    SizedBox(
                      height: screenSize.height * 0.1,
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
