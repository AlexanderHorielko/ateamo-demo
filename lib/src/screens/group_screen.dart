import 'package:flutter/material.dart';
import 'package:ateamo_mobile/src/widgets/app_bar.dart';

class GroupScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppbar(label: 'Groups'),
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        child: Center(
          child: Text('GroupScreen'),
        ),
      ),
    );
  }
}
