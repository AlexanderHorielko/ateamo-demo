import 'package:ateamo_mobile/src/bloc/schedule_bloc/schedule_bloc.dart';
import 'package:ateamo_mobile/src/bloc/schedule_bloc/schedule_event.dart';
import 'package:ateamo_mobile/src/bloc/schedule_bloc/schedule_state.dart';
import 'package:ateamo_mobile/src/models/schedule_bloc_model.dart';
import 'package:ateamo_mobile/src/widgets/schedule/daily_matches_card.dart';
import 'package:flutter/material.dart';

import 'package:ateamo_mobile/src/widgets/app_bar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ScheduleScreen extends StatefulWidget {
  @override
  _ScheduleScreenState createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen> {
  ScheduleBloc _scheduleBloc;
  @override
  void initState() {
    super.initState();
    _scheduleBloc = BlocProvider.of<ScheduleBloc>(context);
    _scheduleBloc.add(ScheduleEventGetMatches());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppbar(label: 'Schedule'),
      backgroundColor: Theme.of(context).backgroundColor,
      body: BlocBuilder<ScheduleBloc, ScheduleState>(
        builder: (context, state) {
          if (state.model.dailyMatches == null) {
            return Center(child: CircularProgressIndicator());
          }
          final ScheduleBlocModel model = state.props[0];
          final dailyMatchesKeys = [];
          model.dailyMatches.forEach((key, value) {
            dailyMatchesKeys.add(key);
          });
          return Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.03,
              left: MediaQuery.of(context).size.height * 0.015,
              right: MediaQuery.of(context).size.height * 0.015,
            ),
            child: ListView.builder(
              itemCount: model.dailyMatches.length,
              itemBuilder: (context, index) {
                return DailyMatchesCard(
                  matches: model.dailyMatches[dailyMatchesKeys[index]],
                  date: dailyMatchesKeys[index],
                );
              },
            ),
          );
        },
      ),
    );
  }
}
