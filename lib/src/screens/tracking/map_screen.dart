import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:ateamo_mobile/src/widgets/app_bar.dart';
import 'package:ateamo_mobile/src/widgets/tracking/map.dart';

class MapScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppbar(label: 'Map',),
      body: TrackingMap(),
    );
  }
}

