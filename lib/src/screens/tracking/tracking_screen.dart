import 'package:ateamo_mobile/src/widgets/app_bar.dart';
import 'package:flutter/material.dart';

import 'package:ateamo_mobile/src/widgets/tracking/location_tracking.dart';

class TrackingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: MyAppbar(label: 'Tracking',),
        backgroundColor: Theme.of(context).backgroundColor,
        body: Center(child: LocationTrackingWidget(),),
      ),
    );
  }
}

