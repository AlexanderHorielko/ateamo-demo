import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_bloc.dart';
import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_event.dart';
import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_state.dart';
import 'package:ateamo_mobile/src/resources/global_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ateamo_mobile/src/widgets/app_bar.dart';
import 'package:ateamo_mobile/src/widgets/activity/activity_card.dart';
import 'package:ateamo_mobile/src/widgets/activity/event_reminder.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  EventReminderBloc eventReminderBloc;
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    eventReminderBloc = BlocProvider.of<EventReminderBloc>(context);
    eventReminderBloc.add(EventReminderEventGetData());
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppbar(scrollController: _scrollController),
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
        child: Center(
          child: BlocBuilder<EventReminderBloc, EventReminderState>(
            builder: (context, state) {
              return RefreshIndicator(
                color: GlobalColors.green,
                onRefresh: () async {
                  eventReminderBloc.add(EventReminderEventGetData());
                  return eventReminderBloc.firstWhere(
                    (element) => !element.loading,
                  );
                },
                child: ListView(
                  controller: _scrollController,
                  children: [
                    Column(
                      children: [
                        for (var i = 0; i < state.model.reminders.length; i++)
                          EventReminderCard(i),
                        for (var i = 0; i < state.model.posts.length; i++)
                          ActivityCard(i),
                      ],
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
