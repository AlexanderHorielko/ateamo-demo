import 'package:ateamo_mobile/src/bloc/event_reminder_bloc/event_reminder_bloc.dart';
import 'package:ateamo_mobile/src/bloc/schedule_bloc/schedule_bloc.dart';
import 'package:ateamo_mobile/src/packages/event_reminder_repository/lib/event_reminder_repository.dart';
import 'package:ateamo_mobile/src/packages/schedule_repository/lib/schedule_repository.dart';
import 'package:ateamo_mobile/src/screens/tracking/map_screen.dart';
import 'package:ateamo_mobile/src/screens/tracking/tracking_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:ateamo_mobile/src/bloc/navigation_bloc/bloc.dart';
import 'package:ateamo_mobile/src/screens/home_screen.dart';
import 'package:ateamo_mobile/src/screens/group_screen.dart';
import 'package:ateamo_mobile/src/screens/schedule_screen.dart';
import 'package:ateamo_mobile/src/widgets/bottom_navigation.dart';

import '../bloc/navigation_bloc/bloc.dart';

class InitialScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final scheduleBloc = ScheduleBloc(
     scheduleRepository: RepositoryProvider.of<ScheduleRepository>(context),
    ); 

    return BlocProvider.value(
      value: NavigationBloc(),
      child: Scaffold(
        body: BlocBuilder<NavigationBloc, NavigationState>(
          builder: (context, state) {
            if (state is HomePageLoadedState) {
              return HomeScreen();
            } else if (state is TrackingPageLoadedState) {
              return TrackingScreen();
            } else if (state is SchedulePageLoadedState) {
              return BlocProvider.value(
                value: scheduleBloc,
                child: ScheduleScreen(),
              );
            } else if (state is GroupPageLoadedState) {
              return GroupScreen();
            } else if (state is MapPageLoadedState) {
              return MapScreen();
            } else {
              return Container();
            }
          },
        ),
        bottomNavigationBar: BottomNavigation(),
      ),
    );
  }
}
