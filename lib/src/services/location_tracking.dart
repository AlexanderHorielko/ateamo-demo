import 'package:ateamo_mobile/src/models/location_row.dart';
import 'package:ateamo_mobile/src/packages/location_repository/lib/location_repository.dart';
import 'package:intl/intl.dart';
import 'package:background_location/background_location.dart';
import 'package:ateamo_mobile/src/models/location_info.dart';

class LocationTracking {
  LocationRepository _locationRepository = LocationRepository();

  void init() async {
    await _locationRepository.init();
    BackgroundLocation.getPermissions(
      onGranted: () {

        BackgroundLocation.getLocationUpdates((location) {
          LocationInfo locationInfo = LocationInfo(
            speed: location.speed,
            time: location.time,
            lat: location.latitude,
            long: location.longitude,
          );

          DateTime today = DateTime.now();
          DateFormat formatter = DateFormat('yyyy-MM-dd');
          String todayFormatted = formatter.format(today);

          LocationRow row = LocationRow(locationInfo: locationInfo, date: todayFormatted);

          _locationRepository.insertRow(row);
        });
      },
      onDenied: () {
        print('DENIED');
      }
    );
  }

  Future<bool> start() async {
    dynamic permissions = await BackgroundLocation.checkPermissions();

    if (permissions.toString() == 'PermissionStatus.granted') {
      BackgroundLocation.startLocationService();

      return true;
    } else {
      return false;
    }
  }

  void stop() {
    BackgroundLocation.stopLocationService();
  }
}
