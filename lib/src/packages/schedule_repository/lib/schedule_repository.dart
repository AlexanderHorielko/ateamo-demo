import 'dart:convert';

import 'package:ateamo_mobile/src/models/event_reminder/match_card_model.dart';
import 'package:ateamo_mobile/src/packages/http_client/lib/http_client.dart';

class ScheduleRepository {
  HttpClient _httpClient = HttpClient();

  Future<List<MatchCardModel>> getAllMathces() async {
    List<MatchCardModel> matches = [];
    final res = await _httpClient.get('schedule');
    List data = jsonDecode(res.body);
    for (var i = 0; i < data.length; i++) {
      for (var j = 0; j < data[i]['events'].length; j++) {
        matches.add(
          MatchCardModel.fromJson(data[i]['events'][j]),
        );
      }
    }
    return matches;
  }
}
