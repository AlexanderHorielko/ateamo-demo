import 'dart:convert';

import 'package:ateamo_mobile/src/models/event_reminder/post_model.dart';
import 'package:ateamo_mobile/src/models/event_reminder/reminder_model.dart';
import 'package:ateamo_mobile/src/packages/http_client/lib/http_client.dart';

class EventReminderRepository {
  HttpClient _httpClient = HttpClient();

  Future<Map<String, dynamic>> getActivities() async {
    List<PostModel> posts = [];
    List<ReminderModel> reminders = [];

    final res = await _httpClient.get('activity');
    final List data = jsonDecode(res.body);
    final postsJson = data.where((e) => e['type'] == 'post').toList();
    final remindersJson = data.where((e) => e['type'] == 'reminder').toList();

    for (var post in postsJson) {
      posts.add(PostModel.fromJson(post));
    }

    for (var reminder in remindersJson) {
      reminders.add(ReminderModel.fromJson(reminder));
    }

    return {
      'posts': posts,
      'reminders': reminders,
    };
  }
}
