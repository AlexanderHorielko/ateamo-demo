import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'package:ateamo_mobile/src/models/location_row.dart';

class LocationRepository {
  Database _database;

  Future<void> init() async {
    try {
      _database = await openDatabase(
        join(await getDatabasesPath(), 'location_database.db'),
        onCreate: (db, version) {
          return db.execute(
            "CREATE TABLE location_row(id INTEGER PRIMARY KEY, speed REAL, time REAL, lat REAL, long REAL, date TEXT)",
          );
        },
        version: 1,
      );
    } catch (error) {
      print(error);
    }
  }

  Future<void> insertRow(LocationRow row) async {
    try {
      await _database.insert('location_row', row.toMap());
    } catch (error) {
      print(error);
    }
  }

  Future<List<LocationRow>> retrieveData() async {
    List<Map<String, dynamic>> rawData = await _database.query('location_row');

    return rawData.map((row) => LocationRow.fromMap(row)).toList();
  }
}
