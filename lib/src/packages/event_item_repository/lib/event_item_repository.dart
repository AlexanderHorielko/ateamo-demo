import 'dart:convert';

import 'package:ateamo_mobile/src/models/event/event_item.dart';
import 'package:ateamo_mobile/src/packages/http_client/lib/http_client.dart';

class EventItemRepository {
  final HttpClient _httpClient = HttpClient();

  Future<EventItem> getEventData({int eventId = 1}) async {
    final jsonData = await _httpClient.get('event/$eventId');
    final data = jsonDecode(jsonData.body);
    final event = EventItem.fromJson(data);
    return event;
  }
}
