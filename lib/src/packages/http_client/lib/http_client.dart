import 'package:http/http.dart' as http;

class HttpClient{
  final String _routeName =
      'https://86251a7d-e4fa-4bb8-a694-c5401526207c.mock.pstmn.io/';

  Future<http.Response> get(String path) async {
    final res = http.get(_routeName + path);
    return res;
  }
}