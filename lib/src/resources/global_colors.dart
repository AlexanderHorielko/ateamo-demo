import 'package:flutter/material.dart';

class GlobalColors {
  static const Color green = Color.fromRGBO(37, 99, 44, 1);
  static const Color grey = Color.fromRGBO(136, 136, 136, 1);
  static const Color black = Color.fromRGBO(21, 21, 21, 1);
  static const Color darkGrey = Color.fromRGBO(85, 85, 85, 1);
  static const Color lightGrey = Color.fromRGBO(246, 246, 246, 1);
  static const Color eventHeaderGrey = Color.fromRGBO(204, 204, 204, 1);
  static const Color teamLabelGrey = Color.fromRGBO(188, 188, 188, 1);
  static const Color activityHeaderGrey = Color.fromRGBO(153, 153, 153, 1);
}
