import UIKit
import Flutter
import GoogleMaps
import WatchConnectivity

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate, WCSessionDelegate {
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        NSLog("received message: \(message)")
    }
    
    var session: WCSession?
    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
    GMSServices.provideAPIKey("AIzaSyDRf9jWXvZbfzJNkyOnRdOqY8GlfzBPSr4")
// method channel
    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
    let channel = FlutterMethodChannel(name: "myWatchChannel", binaryMessenger: controller.binaryMessenger);
    
    channel.setMethodCallHandler({
        (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
        
        guard call.method == "getBatteryLevel" else {
            result(FlutterMethodNotImplemented)
            return
          }
        self.receiveBatteryLevel(result: result)
    })
// end method channel
    self.configureWatchKitSesstion()
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    private func configureWatchKitSesstion() {
        
        if WCSession.isSupported() {//4.1
          session = WCSession.default//4.2
          session?.delegate = self//4.3
          session?.activate()//4.4
        }
      }
    
    private func receiveBatteryLevel(result: FlutterResult) {
      let device = UIDevice.current
      device.isBatteryMonitoringEnabled = true
      if device.batteryState == UIDevice.BatteryState.unknown {
        result(FlutterError(code: "UNAVAILABLE",
                            message: "Battery info unavailable",
                            details: nil))
      } else {
        result(Int(device.batteryLevel * 100))
      }
    }
}
