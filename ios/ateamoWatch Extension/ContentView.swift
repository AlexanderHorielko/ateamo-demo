//
//  ContentView.swift
//  ateamoWatch Extension
//
//  Created by Alexander on 06.04.2021.
//

import SwiftUI
import WatchConnectivity

struct ContentView: View {
    @EnvironmentObject var controller = InterfaceController()
    var body: some View {
        ScrollView{
            VStack(alignment: .center, spacing: 10){
                HStack(alignment: .center, spacing: 10){
                    Button(action: {
                        controller.tapSendToiPhone()
                    }, label: {
                        Text("Button")
                    })
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

